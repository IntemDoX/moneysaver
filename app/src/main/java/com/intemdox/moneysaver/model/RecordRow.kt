package com.intemdox.moneysaver.model


data class RecordRow(
    var id: Int,
    var isIncome: Boolean,
    var date: String,
    var categoryId: Int,
    var count: String,
    var comment: String? = null
)

fun RecordRow.toRecord(): Record {
    return Record(
        id = id,
        isIncome = isIncome,
        date = 9,//todo add read date
        categoryId = categoryId,
        count = count.toDouble(),
        comment = comment
    )
}