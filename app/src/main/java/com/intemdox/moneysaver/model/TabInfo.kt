package com.intemdox.moneysaver.model

data class TabInfo(
    val tabId: String,
    val name: String,
    var totalSpent: Double = 0.0,
    var totalIncome: Double = 0.0,
    val balance: Double = 0.0,
    var records: MutableList<Record>
)