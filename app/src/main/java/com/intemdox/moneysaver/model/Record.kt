package com.intemdox.moneysaver.model

import com.intemdox.moneysaver.utils.toShortDate

data class Record(
    var id: Int,
    var isIncome: Boolean,
    var date: Long,
    var categoryId: Int,
    var count: Double,
    var comment: String? = null
) {
    constructor() : this(0, false, 0, 0, 0.0, null) {}
}

fun Record.toRecordRow(): RecordRow {
    return RecordRow(
        id = id,
        isIncome = isIncome,
        date = date.toShortDate(),
        categoryId = categoryId,
        count = count.toString(),
        comment = comment
    )
}

fun Record.toParamsMap(): MutableMap<String, Any?> {
    return mutableMapOf(
        Pair("id", id),
        Pair("date", date),
        Pair("income", isIncome),
        Pair("categoryId", categoryId),
        Pair("count", count),
        Pair("comment", comment)
    )
}