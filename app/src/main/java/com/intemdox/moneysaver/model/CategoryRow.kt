package com.intemdox.moneysaver.model


data class  CategoryRow(var id: Int = 0, var color: String, var name: String, var isChecked: Boolean = false) {
}