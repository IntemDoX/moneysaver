package com.intemdox.moneysaver.model

import android.util.Log

data class Category(var id: Int = 0, var color: String, var name: String) {
    constructor() : this(0, "", "")
}

fun String.generateIdFromColor(): Int {
    Log.d("testtest", "color hex = ${substring(1)}")
    val a = substring(1).toInt(radix = 16)
    Log.d("testtest", "color int = $a")
    return a
}

fun Category.toCategoryRow(): CategoryRow {
    return CategoryRow(
        id = id,
        color = color,
        name = name,
        isChecked = false
    )
}