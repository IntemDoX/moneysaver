package com.intemdox.moneysaver.navigator

import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import com.intemdox.moneysaver.R
import javax.inject.Inject

class AppNavigatorImpl @Inject constructor(private val activity: FragmentActivity) : AppNavigator {

    override fun navigateTo(screen: Screens) {
        val destination = when (screen) {
            Screens.HOME -> R.id.homeFragment
            Screens.LOGIN -> R.id.loginFragment
            Screens.CATEGORIES -> R.id.categoriesFragment
            Screens.SETTINGS -> R.id.settingsFragment
        }

        with(activity.findNavController(R.id.nav_host_fragment)) {
            graph = navInflater.inflate(R.navigation.nav_graph).apply {
                startDestination = destination
            }
        }
    }
}
