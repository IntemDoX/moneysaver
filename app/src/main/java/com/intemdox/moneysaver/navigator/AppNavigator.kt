package com.intemdox.moneysaver.navigator

enum class Screens {
    LOGIN,
    HOME,
    CATEGORIES,
    SETTINGS
}

interface AppNavigator {
    // Navigate to a given screen.
    fun navigateTo(screen: Screens)
}
