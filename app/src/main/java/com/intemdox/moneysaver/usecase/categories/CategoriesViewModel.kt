package com.intemdox.moneysaver.usecase.categories

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.intemdox.moneysaver.*
import com.intemdox.moneysaver.base.BaseViewModel
import com.intemdox.moneysaver.base.ComponentData
import com.intemdox.moneysaver.base.InternalError
import com.intemdox.moneysaver.coroutines.uiJob
import com.intemdox.moneysaver.model.Category

class CategoriesViewModel @ViewModelInject constructor(private var categoriesInteractor: CategoriesInteractor) :
    BaseViewModel<CategoriesState, CategoriesActions>() {
    override fun initialState() = CategoriesState.EmptyState

    fun createCategory(category: Category) =
        viewModelScope.uiJob {
            when (val result = categoriesInteractor.createCategory(category = category)) {
                is CreateCategorySuccess -> {
                    updateState(CategoriesState.CreateCategorySuccess(ComponentData.Data(readyData = result.category)))
                }
                is CreateCategoryError -> {
                    updateState(CategoriesState.CreateCategoryError(ComponentData.Error(error = InternalError.Throwed(Exception()))))
                }
            }
        }

    fun removeCategory(category: Category) {
        viewModelScope.uiJob {
            when (val result = categoriesInteractor.removeCategory(category = category)) {
                is RemoveCategorySuccess -> {
                }
                is RemoveCategoryError -> {
                }
            }
        }
    }

    fun updateCategory(category: Category, params: HashMap<String, Any>) {
        viewModelScope.uiJob {
            when (val result = categoriesInteractor.updateCategory(category = category, params = params)) {
                is UpdateCategorySuccess -> {

                }
                is UpdateCategoryError -> {

                }
            }
        }
    }
}