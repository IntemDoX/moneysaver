package com.intemdox.moneysaver.usecase.home

import com.intemdox.moneysaver.base.BaseViewAction
import com.intemdox.moneysaver.base.BaseViewState
import com.intemdox.moneysaver.model.TabInfo


sealed class HomeState : BaseViewState {
    object EmptyState : HomeState()
    class UpdateTabInfoState(var tabInfo: TabInfo) : HomeState()
    class InitTabsState(var tabsList: MutableList<TabInfo>) : HomeState()
    object RecordCreatedSuccessState : HomeState()
    class RecordCreatedFailureState(val e: Throwable) : HomeState()
    object RecordUpdatedSuccessState : HomeState()
    class RecordUpdatedFailureState(val e: Throwable) : HomeState()
    class RecordsAddedState(var changedTab: TabInfo) : HomeState()
    class RecordChangeState(var changedTab: TabInfo) : HomeState()
    class RecordRemovedState(var changedTab: TabInfo) : HomeState()
    class NewTabAddedState(var changedTab: TabInfo, var tabList: MutableList<TabInfo>) : HomeState()
    class TabRemovedState(var tabList: MutableList<TabInfo>) : HomeState()
}

sealed class HomeActions : BaseViewAction