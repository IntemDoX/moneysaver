package com.intemdox.moneysaver.usecase.home

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.viewModelScope
import com.intemdox.moneysaver.*
import com.intemdox.moneysaver.base.BaseViewModel
import com.intemdox.moneysaver.coroutines.uiJob
import com.intemdox.moneysaver.model.Record
import com.intemdox.moneysaver.model.TabInfo
import com.intemdox.moneysaver.usecase.categories.CategoriesInteractor
import com.intemdox.moneysaver.utils.toTabId
import com.intemdox.moneysaver.utils.toTabName
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import java.util.*
import kotlin.coroutines.CoroutineContext

class HomeViewModel @ViewModelInject constructor(var recordsInteractorImpl: RecordsInteractor, var categoriesInteractor: CategoriesInteractor) :
    BaseViewModel<HomeState, HomeActions>() {

    lateinit var tabList: MutableList<TabInfo>

    override fun initialState() = HomeState.EmptyState

    private fun onNewAdded(record: Record) {
        Log.d("testtest", "OnNewAdded $record")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = record.date
        val tabName = calendar.toTabName()
        val tabId = calendar.toTabId()
        var changedTab: TabInfo? = null
        //todo there was crash. Tablist was null
        for (tab in tabList) {
            if (tab.tabId == tabId) {
                tab.records.add(record)
                if (record.isIncome) {
                    tab.totalIncome += record.count
                } else {
                    tab.totalSpent += record.count
                }
                changedTab = tab
            }
        }
        if (changedTab == null) {
            val tab = TabInfo(
                tabId = tabId,
                name = tabName,
                totalSpent = if (record.isIncome) 0.0 else record.count,
                totalIncome = if (record.isIncome) record.count else 0.0,
                records = mutableListOf(record)
            )
            tabList.add(tab)
            updateState(HomeState.NewTabAddedState(tab, tabList))
        } else {
            updateState(HomeState.RecordsAddedState(changedTab))
        }
    }

    private fun onModified(record: Record) {
        Log.d("testtest", "OnModified $record")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = record.date
        val tabId = calendar.toTabId()
        var changedTab: TabInfo? = null
        for (tab in tabList) {
            if (tab.tabId == tabId) {
                for (record in tab.records) {
                    if (record.id == record.id) {
                        record.count = record.count
                        record.categoryId = record.categoryId
                        record.isIncome = record.isIncome
                        record.comment = record.comment
                        record.date = record.date
                    }
                }
                if (record.isIncome) {//todo maybe excess changes will appear (for example additional adding totalIncome or totalSpent if they weren't changed. Change it only if this fields were changed
                    tab.totalIncome += record.count
                } else {
                    tab.totalSpent += record.count
                }
                changedTab = tab
            }
        }
        updateState(HomeState.RecordChangeState(changedTab!!))
    }


    private fun onModifiedWithDate(record: Record) {
        onRemoved(record)
        onNewAdded(record)
    }

    private fun onRemoved(record: Record) {
        Log.d("testtest", "OnRemoved $record")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = record.date
        val tabId = calendar.toTabId()
        var changedTab: TabInfo? = null
        for (tab in tabList) {
            if (tab.tabId == tabId) {
                tab.records.remove(record)
                if (record.isIncome) {
                    tab.totalIncome -= record.count
                } else {
                    tab.totalSpent -= record.count
                }
                changedTab = tab
            }
        }
        if (changedTab!!.records.isEmpty()) {
            tabList.remove(changedTab)
            updateState(HomeState.TabRemovedState(tabList))
        } else {
            updateState(HomeState.RecordRemovedState(changedTab!!))
        }
    }

    init {
        viewModelScope.uiJob {
            recordsInteractorImpl.subscribeOnRecordsChanges(
                onNewAdded = {
                    onNewAdded(it)
                },
                onModified = {
                    onModified(it)
                },
                onModifiedWithDate = {
                    //need to remove object from current list(removeEven maybe) and add object to new list(add event maybe)
                    onModifiedWithDate(it) //todo check if it works
                },
                onRemoved = {
                    onRemoved(it)
                }
            )
        }
        loadData()
    }


    private fun loadData() = viewModelScope.uiJob {
        val categories = categoriesInteractor.getCategories()
        val result = recordsInteractorImpl.getAllRecords()
        when (result) {
            is GetAllRecordsSuccess -> {
                tabList = result.records
                updateState(HomeState.InitTabsState(tabList))
            }
            is GetAllRecordsError -> {
                updateState(HomeState.EmptyState)

            }
        }
    }

    fun createNewRecord(record: Record) = viewModelScope.uiJob {
        when (val result = recordsInteractorImpl.createNewRecord(record)) {
            is CreateNewRecordSuccess -> {
                updateState(HomeState.RecordCreatedSuccessState)
            }
            is CreateNewRecordFailure -> {
                updateState(HomeState.RecordCreatedFailureState(result.e))
            }
        }
    }

    fun updateRecord(id: String, params: MutableMap<String, Any>) = viewModelScope.uiJob {
        when (val result = recordsInteractorImpl.updateRecord(id, params)) {
            is UpdateRecordSuccess -> {
                updateState(HomeState.RecordUpdatedSuccessState)
            }
            is UpdateRecordError -> {
                updateState(HomeState.RecordUpdatedFailureState(result.e))
            }
        }
    }

    fun removeRecord(id: String) = viewModelScope.uiJob {
        when (val result = recordsInteractorImpl.removeRecord(id)) {
            is RemoveRecordSuccess -> {
                updateState(HomeState.RecordUpdatedSuccessState)
            }
            is RemoveRecordError -> {
                updateState(HomeState.RecordUpdatedFailureState(result.e))
            }
        }
    }

    fun lt(place: String) {
        Log.d("testtest", "$place is executing on thread : ${Thread.currentThread().name}")
    }

    fun getDispatcherDefault(threadName: String? = null): CoroutineContext {
        threadName?.let {
            return Dispatchers.Default + CoroutineName(threadName)
        }
        return Dispatchers.Default
    }

    fun getDispatcherMain(threadName: String? = null): CoroutineContext {
        threadName?.let {
            return Dispatchers.Main + CoroutineName(threadName)
        }
        return Dispatchers.Main
    }

    fun getDispatcherIO(threadName: String? = null): CoroutineContext {
        threadName?.let {
            return Dispatchers.IO + CoroutineName(threadName)
        }
        return Dispatchers.IO
    }


}
