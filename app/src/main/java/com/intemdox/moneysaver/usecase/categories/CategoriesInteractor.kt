package com.intemdox.moneysaver.usecase.categories

import com.intemdox.moneysaver.CreateCategoryResult
import com.intemdox.moneysaver.GetAllCategoriesResult
import com.intemdox.moneysaver.RemoveCategoryResult
import com.intemdox.moneysaver.UpdateCategoryResult
import com.intemdox.moneysaver.model.Category

interface CategoriesInteractor {
    suspend fun createCategory(category: Category): CreateCategoryResult
    suspend fun updateCategory(category: Category, params: MutableMap<String, Any>): UpdateCategoryResult
    suspend fun removeCategory(category: Category): RemoveCategoryResult
    suspend fun getCategories(): GetAllCategoriesResult
    suspend fun subscribeOnCategoriesChanges(
        onNewAdded: (category: Category) -> Unit,
        onModified: (category: Category) -> Unit,
        onRemoved: (category: Category) -> Unit
    )
}