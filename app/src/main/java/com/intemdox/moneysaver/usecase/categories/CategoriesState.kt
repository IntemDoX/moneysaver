package com.intemdox.moneysaver.usecase.categories

import com.intemdox.moneysaver.base.BaseViewAction
import com.intemdox.moneysaver.base.BaseViewState
import com.intemdox.moneysaver.base.ComponentData
import com.intemdox.moneysaver.model.Category

sealed class CategoriesState : BaseViewState {
    object EmptyState : CategoriesState()
    data class CreateCategorySuccess(var data: ComponentData.Data<Category>) : CategoriesState()
    data class CreateCategoryError(var error: ComponentData.Error<Throwable>) : CategoriesState()
    data class RemoveCategorySuccess(var data: ComponentData.Data<Category>) : CategoriesState()
    data class RemoveCategoryError(var error: ComponentData.Error<Throwable>) : CategoriesState()
    data class UpdateCategorySuccess(var data: ComponentData.Data<Category>) : CategoriesState()
    data class UpdateCategoryError(var error: ComponentData.Error<Throwable>) : CategoriesState()
}


sealed class CategoriesActions : BaseViewAction