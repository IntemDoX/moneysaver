package com.intemdox.moneysaver.usecase.categories

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.intemdox.moneysaver.R
import com.intemdox.moneysaver.databinding.BottomsheetCreateCategoryBinding
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.model.generateIdFromColor
import com.intemdox.moneysaver.utils.ItemDecorator
import com.intemdox.moneysaver.utils.setGone
import com.intemdox.moneysaver.utils.setVisible

class CreateCategoryBottomSheet @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding = BottomsheetCreateCategoryBinding.inflate(LayoutInflater.from(context), this)
    lateinit var adapter: ColorsAdapter

    init {
        background = context.getDrawable(R.drawable.top_rounded_background)
        initRecyclerView()
    }

    fun getCategory(): Category? {
        val text = StringBuilder()
        if (binding.name.text.isNullOrEmpty()) {
            text.append("Fill category name/n")
        }
        if (adapter.selectedColor.isNullOrEmpty()) {
            text.append("Fill category color")
        }
        if (text.isNotEmpty()) {
            binding.errorText.setVisible()
            binding.errorText.text = text.toString()
            return null
        } else {
            binding.errorText.setGone()
        }
        return Category(adapter.selectedColor.generateIdFromColor(), adapter.selectedColor, binding.name.text.toString())
    }

    private fun initRecyclerView() {
        adapter = ColorsAdapter()
        adapter.clickListener = { category ->
            Log.d("testtest", "category = $category")
        }
        binding.colorsList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.colorsList.addItemDecoration(ItemDecorator())
        binding.colorsList.adapter = adapter
    }
}