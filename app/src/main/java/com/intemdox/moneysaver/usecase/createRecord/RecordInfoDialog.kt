package com.intemdox.moneysaver.usecase.createRecord

import android.app.DatePickerDialog
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.intemdox.moneysaver.repository.FirestoreRepository
import com.intemdox.moneysaver.R
import com.intemdox.moneysaver.databinding.DialogEditRecordBinding
import com.intemdox.moneysaver.model.Record
import com.intemdox.moneysaver.utils.setGone
import com.intemdox.moneysaver.utils.setVisible
import com.intemdox.moneysaver.utils.toShortDate
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class RecordInfoDialog : DialogFragment() {

    private lateinit var binding: DialogEditRecordBinding

    @Inject
    lateinit var adapter: CategoriesAdapter

    @Inject
    lateinit
    var firestoreRepositoryImpl: FirestoreRepository

    var dateAndTime = Calendar.getInstance()

    var categoryId = 0

    companion object {
        fun getInstance(record: Record): RecordInfoDialog {
            val dialog = RecordInfoDialog()
            val bundle = bundleOf(
                Pair(KEY_COMMENT, record.comment),
                Pair(KEY_RECORD_ID, record.id),
                Pair(KEY_CATEGORY_ID, record.categoryId),
                Pair(KEY_ISINCOME, record.isIncome),
                Pair(KEY_DATE, record.date),
                Pair(KEY_AMOUNT, record.count)
            )
            dialog.arguments = bundle
            return dialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogEditRecordBinding.inflate(inflater)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setWindowAnimations(
            R.style.DialogAnimation
        )
        val layoutManager = FlexboxLayoutManager(requireContext())
        layoutManager.flexDirection = FlexDirection.ROW
        val categoriesList = firestoreRepositoryImpl.categories
        adapter.setItems(categoriesList)
        binding.category.setOnClickListener {
            binding.categoriesRecycler.setVisible()
        }
        adapter.clickListener = { category ->
            category?.let {
                Log.d("testtest", "choosed category id = ${it.id}")
                categoryId = it.id
                binding.categoriesRecycler.setGone()
                binding.category.text = firestoreRepositoryImpl.getCategoryById(categoryId.toString())!!.name
                val layerList = binding.category.background as GradientDrawable
                layerList.setColor(Color.parseColor(firestoreRepositoryImpl.getCategoryById(categoryId.toString())!!.color))
            }
        }

        val bundle = requireArguments()

        val amount = bundle.getDouble(KEY_AMOUNT)
        val id = bundle.getInt(KEY_RECORD_ID)
        val comment = bundle.getString(KEY_COMMENT, "")
        val isIncome = bundle.getBoolean(KEY_ISINCOME)
        val bcategoryId = bundle.getInt(KEY_CATEGORY_ID)
        val date = bundle.getLong(KEY_DATE)

        binding.amount.setText(amount.toString())
        binding.comment.setText(comment)
        binding.isIncome.isChecked = isIncome
        binding.date.text = date.toShortDate()
        dateAndTime.timeInMillis = date
        val cat = firestoreRepositoryImpl.getCategoryById(bcategoryId.toString())!!
        binding.category.text = cat.name
        val layerList = binding.category.background as GradientDrawable
        layerList.setColor(Color.parseColor(cat.color))

        binding.categoriesRecycler.layoutManager = layoutManager
        binding.categoriesRecycler.setHasFixedSize(true)
        binding.categoriesRecycler.isNestedScrollingEnabled = true
        binding.categoriesRecycler.adapter = adapter
        binding.date.text = Date().time.toShortDate()
        val d =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                dateAndTime.set(Calendar.YEAR, year)
                dateAndTime.set(Calendar.MONTH, monthOfYear)
                dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                setInitialDateTime()
            }
        binding.date.setOnClickListener {
            DatePickerDialog(
                requireContext(), d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH)
            )
                .show()
        }
        binding.btnRemove.setOnClickListener {
            val bundle = bundleOf(
                Pair(KEY_RECORD_ID, id)
            )
            parentFragmentManager.setFragmentResult("RecordInfoDialogRemove", bundle)
            dismiss()
        }
        binding.btnSave.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(KEY_RECORD_ID, id)
            if (categoryId != cat.id) {
                Log.d("testtest2", "add categoryId")
                bundle.putInt(KEY_CATEGORY_ID, categoryId)
            }
            if (binding.isIncome.isChecked != isIncome) {
                bundle.putBoolean(KEY_ISINCOME, binding.isIncome.isChecked)
            }
            if (dateAndTime.timeInMillis != date) {
                bundle.putLong(KEY_DATE, dateAndTime.timeInMillis)
            }
            if (amount != binding.amount.text.toString().toDouble()) {
                Log.d("testtest2", "add amount")
                bundle.putDouble(KEY_AMOUNT, binding.amount.text.toString().toDouble())
            }
            if (comment != binding.comment.text.toString()) {
                bundle.putString(KEY_COMMENT, binding.comment.text.toString())
            }
//            val bundle = bundleOf(
//                Pair(KEY_COMMENT, binding.comment.text.toString()),
//                Pair(KEY_CATEGORY_ID, categoryId),
//                Pair(KEY_RECORD_ID, id),
//                Pair(KEY_ISINCOME, binding.isIncome.isChecked),
//                Pair(KEY_DATE, dateAndTime.timeInMillis),
//                Pair(KEY_AMOUNT, binding.amount.text.toString().toDouble())
//            )
            parentFragmentManager.setFragmentResult(RecordInfoDialog::class.java.simpleName, bundle)
            dismiss()
        }
    }

    fun setInitialDateTime() {
        binding.date.text = dateAndTime.timeInMillis.toShortDate()
    }
}