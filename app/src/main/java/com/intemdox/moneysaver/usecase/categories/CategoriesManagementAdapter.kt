package com.intemdox.moneysaver.usecase.categories

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.intemdox.moneysaver.databinding.ItemCategoriesManagementBinding
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.utils.DiffCallbackCategories
import javax.inject.Inject


class CategoriesManagementAdapter @Inject constructor() :
    RecyclerView.Adapter<CategoriesManagementAdapter.CategoryViewHolder>() {
    var clickListener: ((Category?) -> Unit)? = null
    private val list: MutableList<Category> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            ItemCategoriesManagementBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(list: MutableList<Category>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun getItemByPosition(position: Int): Category {
        return list[position]
    }

    fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    fun updateList(newList: List<Category>) {
        Log.d("testtest", "newList = ${newList.size} and oldList = ${list.size}")
        val diffResult = DiffUtil.calculateDiff(DiffCallbackCategories(newList, list))
        setItems(newList.toMutableList())
        diffResult.dispatchUpdatesTo(this)
    }

    fun addNewCategory(category: Category) {
        list.add(category)
        notifyItemInserted(list.size - 1)
    }

    fun restoreItem(item: Category, position: Int) {
        list.add(position, item)
        notifyItemInserted(position)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(list[position], clickListener)
        holder.binding.title.text = list[position].name
        clickListener?.let { listener ->
            holder.itemView.setOnClickListener {
                listener(list[position])
            }
        }
    }

    class CategoryViewHolder(var binding: ItemCategoriesManagementBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(category: Category, clickListener: ((Category) -> Unit)?) {
            setColor(category.color)

        }

        private fun setColor(color: String) {
            val layerList = binding.container.background as GradientDrawable
            layerList.setColor(Color.parseColor(color))
        }


    }
}