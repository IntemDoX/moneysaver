package com.intemdox.moneysaver.usecase.settings

import com.intemdox.moneysaver.base.BaseViewModel

class SettingsViewModel : BaseViewModel<SettingsState, SettingsActions>() {
    override fun initialState() = SettingsState.EmptyState


}