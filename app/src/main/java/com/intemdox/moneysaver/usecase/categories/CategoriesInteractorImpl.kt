package com.intemdox.moneysaver.usecase.categories

import android.util.Log
import com.intemdox.moneysaver.*
import com.intemdox.moneysaver.coroutines.backgroundTask
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.repository.*
import javax.inject.Inject

class CategoriesInteractorImpl @Inject constructor(var repository: FirestoreRepository) : CategoriesInteractor {

    override suspend fun createCategory(category: Category): CreateCategoryResult = backgroundTask {
        val data = repository.createCategory(category)
        when (data) {
            is FirebaseCreateCategorySuccess -> return@backgroundTask CreateCategorySuccess(data.category)
            is FirebaseCreateCategoryError -> return@backgroundTask CreateCategoryError(data.e)
        }
    }

    override suspend fun updateCategory(category: Category, params: MutableMap<String, Any>): UpdateCategoryResult = backgroundTask {
        val data = repository.updateCategory(category, params)
        when (data) {
            is FirebaseUpdateCategorySuccess -> {
                return@backgroundTask UpdateCategorySuccess(category)

            }
            is FirebaseUpdateCategoryError -> {
                return@backgroundTask UpdateCategoryError(data.e)
            }
        }
    }

    override suspend fun removeCategory(category: Category): RemoveCategoryResult = backgroundTask {
        val data = repository.removeCategory(category.id.toString())
        data.e?.let {
            return@backgroundTask RemoveCategoryError(it)
        }
        return@backgroundTask RemoveCategorySuccess(category)
    }

    override suspend fun getCategories(): GetAllCategoriesResult = backgroundTask {
        Log.d("testtest", "start get categories")
        val data = repository.getAllCategories()
        when (data) {
            is FirebaseGetCategoriesSuccess -> {
                Log.d("testtest", "get categories success")
                return@backgroundTask GetAllCategoriesSuccess(data.categories)
            }
            is FirebaseGetCategoriesError -> {
                Log.d("testtest", "get categories error")
                return@backgroundTask GetAllCategoriesError(data.e)
            }
        }
    }

    override suspend fun subscribeOnCategoriesChanges(
        onNewAdded: (category: Category) -> Unit,
        onModified: (category: Category) -> Unit,
        onRemoved: (category: Category) -> Unit
    ) {
        repository.subscribeOnCategoriesChanges(onNewAdded, onModified, onRemoved)
    }
}