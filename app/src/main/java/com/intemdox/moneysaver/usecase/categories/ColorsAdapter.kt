package com.intemdox.moneysaver.usecase.categories

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.intemdox.moneysaver.databinding.ItemColorBinding
import javax.inject.Inject

class ColorsAdapter @Inject constructor() :
    RecyclerView.Adapter<ColorsAdapter.ColorViewHolder>() {
    var clickListener: ((String) -> Unit)? = null
    private val list: MutableList<String> = mutableListOf("#2CB632", "#009688", "#BC0606", "#BC0607", "#BC0608", "#BC0609")
    private var checked = -1
     var selectedColor = ""
    var lastSelectedHolder: ColorViewHolder? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        return ColorViewHolder(
            ItemColorBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(list: MutableList<String>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        holder.bind(list[position], clickListener)
        holder.itemView.setOnClickListener {
            if (checked != position) {
                lastSelectedHolder?.let {
                    it.itemView.animate().scaleX(1f)
                    it.itemView.animate().scaleY(1f)
                }
                checked = position
                lastSelectedHolder = holder
                holder.itemView.animate().scaleX(1.5f)
                holder.itemView.animate().scaleY(1.5f)
                selectedColor = list[position]
                Log.d("testtest", "selectedColor = $selectedColor")
            }
        }
    }

    class ColorViewHolder(var binding: ItemColorBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(color: String, clickListener: ((String) -> Unit)?) {
            setColor(color)
            binding.container.setOnClickListener {
                clickListener?.let {
                    it(color)
                }
            }
        }

        private fun setColor(color: String) {
            val layerList = binding.container.background as GradientDrawable
            layerList.setColor(Color.parseColor(color))
        }
    }
}