package com.intemdox.moneysaver.usecase.settings

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.intemdox.moneysaver.base.BaseFragment
import com.intemdox.moneysaver.databinding.FragmentSettingsBinding

class SettingsFragment : BaseFragment<SettingsState, SettingsActions, SettingsViewModel, FragmentSettingsBinding>() {
    override val viewModel: SettingsViewModel by viewModels()
    override var _binding: FragmentSettingsBinding? = null

    override fun applyViewState(viewState: SettingsState) {
        Log.d("testtest", "here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSettingsBinding.inflate(inflater)
        return binding.root
    }
}