package com.intemdox.moneysaver.usecase.createRecord

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.intemdox.moneysaver.R
import com.intemdox.moneysaver.databinding.BottomsheetNavigationMenuBinding

class NavigationBottomSheet @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding = BottomsheetNavigationMenuBinding.inflate(LayoutInflater.from(context), this)

    var onCategoriesClickListener: () -> Unit = {}

    init {
        background = context.getDrawable(R.drawable.top_rounded_background)
//        binding.keyboard.controllableInputField = binding.amountInput
        binding.categories.setOnClickListener { onCategoriesClickListener() }
    }

    fun setCategory() {

    }
}