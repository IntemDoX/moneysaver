package com.intemdox.moneysaver.usecase.settings

import com.intemdox.moneysaver.base.BaseViewAction
import com.intemdox.moneysaver.base.BaseViewState

sealed class SettingsState: BaseViewState{
    object EmptyState: SettingsState()
}

sealed class SettingsActions: BaseViewAction {

}