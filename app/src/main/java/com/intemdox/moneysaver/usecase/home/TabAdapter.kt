package com.intemdox.moneysaver.usecase.home

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.intemdox.moneysaver.databinding.ItemTabBinding
import com.intemdox.moneysaver.model.TabInfo
import com.intemdox.moneysaver.utils.DiffCallbackTabs
import javax.inject.Inject

class TabAdapter @Inject constructor() : RecyclerView.Adapter<TabAdapter.TabViewHolder>() {

    private var tabList = mutableListOf<TabInfo>()
    var clickListener: ((tab: TabInfo) -> Unit)? = null
    lateinit var selectedTab: TabInfo
    private var checked = -1
    var lastSelectedHolder: TabViewHolder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TabViewHolder {
        return TabViewHolder(
            ItemTabBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun setItems(list: MutableList<TabInfo>) {
        this.tabList.clear()
        this.tabList.addAll(list)
        notifyDataSetChanged()
        selectedTab = tabList[tabList.size - 1]
    }

    fun getItems(): MutableList<TabInfo> {
        return tabList
    }

    fun updateTabList(newList: MutableList<TabInfo>) {
        val diffResult = DiffUtil.calculateDiff(DiffCallbackTabs(newList, tabList))
        setItems(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onBindViewHolder(holder: TabViewHolder, position: Int) {
        holder.bind(tabList[position].name)
        holder.itemView.setOnClickListener {
            if (checked != position) {
                selectedTab = tabList[position]
                clickListener?.let { it(tabList[position]) }
                lastSelectedHolder?.let {
                    it.itemView.setBackgroundColor(Color.WHITE)
                }
                checked = position
                lastSelectedHolder = holder
                holder.itemView.setBackgroundColor(Color.RED)
            }
        }
        if (checked == -1 && position == tabList.size - 1) {
            holder.itemView.setBackgroundColor(Color.RED)
            checked = position
            lastSelectedHolder = holder
        }
    }

    override fun getItemCount(): Int {
        return tabList.size
    }

    class TabViewHolder(var binding: ItemTabBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(name: String) {
            binding.title.text = name
        }
    }
}