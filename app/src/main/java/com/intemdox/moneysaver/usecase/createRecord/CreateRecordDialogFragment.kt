package com.intemdox.moneysaver.usecase.createRecord

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.intemdox.moneysaver.repository.FirestoreRepository
import com.intemdox.moneysaver.R
import com.intemdox.moneysaver.databinding.DialogCreateBinding
import com.intemdox.moneysaver.utils.toShortDate
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject


const val KEY_COMMENT = "comment"
const val KEY_RECORD_ID = "id"
const val KEY_AMOUNT = "amount"
const val KEY_ISINCOME = "isIncome"
const val KEY_CATEGORY_ID = "categoryId"
const val KEY_DATE = "date"

@AndroidEntryPoint
class CreateRecordDialogFragment : DialogFragment() {

    private lateinit var binding: DialogCreateBinding

    @Inject
    lateinit var adapter: CategoriesAdapter

    @Inject
    lateinit
    var firestoreRepositoryImpl: FirestoreRepository
    var dateAndTime = Calendar.getInstance()

    var categoryId = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogCreateBinding.inflate(inflater)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setWindowAnimations(
            R.style.DialogAnimation
        )
        val layoutManager = FlexboxLayoutManager(requireContext())
        layoutManager.flexDirection = FlexDirection.ROW
        val categoriesList = firestoreRepositoryImpl.categories
        adapter.setItems(categoriesList)
        adapter.clickListener = { category ->
            category?.let {
                Log.d("testtest", "choosed category id = ${it.id}")
                categoryId = it.id
            }
        }

        binding.categoriesList.layoutManager = layoutManager
        binding.categoriesList.setHasFixedSize(true)
        binding.categoriesList.isNestedScrollingEnabled = true
        binding.categoriesList.adapter = adapter
        binding.date.text = Date().time.toShortDate()
        val d =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                dateAndTime.set(Calendar.YEAR, year)
                dateAndTime.set(Calendar.MONTH, monthOfYear)
                dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                setInitialDateTime()
            }
        binding.date.setOnClickListener {
            DatePickerDialog(
                requireContext(), d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH)
            )
                .show()
        }
        binding.btnCreate.setOnClickListener {
            val bundle = bundleOf(
                Pair(KEY_COMMENT, binding.comment.text.toString()),
                Pair(KEY_CATEGORY_ID, categoryId),
                Pair(KEY_ISINCOME, binding.isIncome.isChecked),
                Pair(KEY_DATE, dateAndTime.timeInMillis),
                Pair(KEY_AMOUNT, binding.count.text.toString().toDouble())
            )
            parentFragmentManager.setFragmentResult(
                CreateRecordDialogFragment::class.java.simpleName,
                bundle
            )
            dismiss()
        }
    }

    fun setInitialDateTime() {
        binding.date.text = dateAndTime.timeInMillis.toShortDate()
    }
}