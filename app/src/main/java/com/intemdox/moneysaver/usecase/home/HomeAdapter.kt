package com.intemdox.moneysaver.usecase.home

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.intemdox.moneysaver.R
import com.intemdox.moneysaver.databinding.ItemRecordBinding
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.model.RecordRow
import com.intemdox.moneysaver.repository.FirestoreRepository
import com.intemdox.moneysaver.utils.DiffCallbackRecords
import com.intemdox.moneysaver.utils.setGone
import com.intemdox.moneysaver.utils.setVisible
import javax.inject.Inject

class HomeAdapter @Inject constructor(var firestoreRepositoryImpl: FirestoreRepository) :
    RecyclerView.Adapter<HomeAdapter.RecordViewHolder>() {
    var clickListener: ((RecordRow) -> Unit)? = null
    private val list: MutableList<RecordRow> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecordViewHolder {
        return RecordViewHolder(
            ItemRecordBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(list: MutableList<RecordRow>) {
        Log.d("testtest", "Set items")
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun getItemByPosition(position: Int): RecordRow {
        return list[position]
    }

    fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    fun updateList(newList: List<RecordRow>) {
        Log.d("testtest", "newList = ${newList.size} and oldList = ${list.size}")
        val diffResult = DiffUtil.calculateDiff(DiffCallbackRecords(newList, list))
        setItems(newList.toMutableList())
        diffResult.dispatchUpdatesTo(this)
    }

    fun restoreItem(item: RecordRow, position: Int) {
        list.add(position, item)
        notifyItemInserted(position)
    }

    override fun onBindViewHolder(holder: RecordViewHolder, position: Int) {
        Log.d("testtest", "HERE 00102102321")
        val category = firestoreRepositoryImpl.getCategoryById(list[position].categoryId.toString())!!
        holder.bind(list[position], category, clickListener)

    }

    class RecordViewHolder(var binding: ItemRecordBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(record: RecordRow, category: Category, clickListener: ((RecordRow) -> Unit)?) {
            with(record) {
                clickListener?.let {
                    itemView.setOnClickListener {
                        it(record)
                    }
                }
                binding.count.text = count
                binding.category.text = category.name
                comment?.run {
                    binding.comment.setVisible()
                    binding.comment.text = this
                } ?: binding.comment.setGone()
                binding.count.setTextColor(binding.count.context.getColor(if (isIncome) R.color.colorGreen else R.color.colorRed))
                binding.date.text = date
                val layerList = binding.category.background as GradientDrawable
                layerList.setColor(Color.parseColor(category.color))

            }
        }
    }
}