package com.intemdox.moneysaver.usecase.categories

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.intemdox.moneysaver.R
import com.intemdox.moneysaver.base.BaseFragment
import com.intemdox.moneysaver.databinding.FragmentCategoriesBinding
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.repository.FirestoreRepository
import com.intemdox.moneysaver.utils.ItemDecorator
import com.intemdox.moneysaver.utils.setGone
import com.intemdox.moneysaver.utils.setVisible
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CategoriesFragment : BaseFragment<CategoriesState, CategoriesActions, CategoriesViewModel, FragmentCategoriesBinding>() {
    @Inject
    lateinit var adapter: CategoriesManagementAdapter

    @Inject
    lateinit var repo: FirestoreRepository //TODO ONLY FOR TEST! REMOVE THIS!
    override val viewModel: CategoriesViewModel by viewModels()
    override var _binding: FragmentCategoriesBinding? = null
    private lateinit var createCategoryBottomSheetBehavior: BottomSheetBehavior<CreateCategoryBottomSheet>

    override fun applyViewState(viewState: CategoriesState) {
        when (viewState) {
            is CategoriesState.EmptyState -> {
            }
            is CategoriesState.CreateCategorySuccess -> {
                Log.d("testtest", "HERe!!!!!!!!!!!!!!!!!112321312312")
                adapter.addNewCategory(viewState.data.readyData)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentCategoriesBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        adapter.setItems(repo.categories)
        initCreateCategoryBottomSheet()
        with(binding) {
            fab.setOnClickListener {
                when (createCategoryBottomSheetBehavior.state) {
                    BottomSheetBehavior.STATE_HALF_EXPANDED,
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        binding.bottomSheet.getCategory()?.let {
                            viewModel.createCategory(it)
                            binding.fab.setImageResource(R.drawable.ic_add)
                            binding.transparentBackground.setGone()
                            createCategoryBottomSheetBehavior.state =
                                BottomSheetBehavior.STATE_HIDDEN
                        }
                    }
                    BottomSheetBehavior.STATE_HIDDEN,
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        binding.fab.setImageResource(R.drawable.ic_done)
                        binding.transparentBackground.setVisible()
                        createCategoryBottomSheetBehavior.state =
                            BottomSheetBehavior.STATE_EXPANDED
                    }
                    BottomSheetBehavior.STATE_DRAGGING,
                    BottomSheetBehavior.STATE_SETTLING -> {
                        //do nothing in this states
                    }
                }
            }
            transparentBackground.setOnClickListener {
                binding.fab.setImageResource(R.drawable.ic_add)
                binding.transparentBackground.setGone()
                createCategoryBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            bottomSheet.setOnClickListener {
                //to avoid hiding on missclick
            }
        }
    }

    private fun openCategoryInfo(category: Category) {
        //todo implement it
    }

    private fun initRecyclerView() {
        adapter.clickListener = { category ->
            openCategoryInfo(category!!)
        }
        binding.categoriesList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.categoriesList.addItemDecoration(ItemDecorator())
        binding.categoriesList.adapter = adapter
    }

    private fun initCreateCategoryBottomSheet() {
        createCategoryBottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)
        createCategoryBottomSheetBehavior.isGestureInsetBottomIgnored = true
        createCategoryBottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                //do nothing for now
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HALF_EXPANDED,
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_HIDDEN,
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING,
                    BottomSheetBehavior.STATE_SETTLING -> {
                        //do nothing in this states
                    }
                }
            }
        })
    }
}