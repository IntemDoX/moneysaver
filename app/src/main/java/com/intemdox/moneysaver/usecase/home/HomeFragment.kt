package com.intemdox.moneysaver.usecase.home

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.intemdox.moneysaver.BottomNavigationDrawerFragment
import com.intemdox.moneysaver.R
import com.intemdox.moneysaver.base.BaseFragment
import com.intemdox.moneysaver.databinding.FragmentHomeBinding
import com.intemdox.moneysaver.model.*
import com.intemdox.moneysaver.navigator.AppNavigator
import com.intemdox.moneysaver.navigator.Screens
import com.intemdox.moneysaver.usecase.createRecord.*
import com.intemdox.moneysaver.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeState, HomeActions, HomeViewModel, FragmentHomeBinding>() {

    override val viewModel: HomeViewModel by viewModels()
    override var _binding: FragmentHomeBinding? = null
    @Inject
    lateinit var navigator: AppNavigator
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<NavigationBottomSheet>

    private val bottomSheetCallback: BottomSheetBehavior.BottomSheetCallback =
        object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                //do nothing for now
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HALF_EXPANDED,
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        binding.fab.setImageResource(R.drawable.ic_done)
                        binding.transparentBackground.setVisible()
                    }
                    BottomSheetBehavior.STATE_HIDDEN,
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        binding.fab.setImageResource(R.drawable.ic_add)
                        binding.transparentBackground.setGone()
                    }
                    BottomSheetBehavior.STATE_DRAGGING,
                    BottomSheetBehavior.STATE_SETTLING -> {
                        //do nothing in this states
                    }
                }
            }
        }

    @Inject
    lateinit var adapter: HomeAdapter

    @Inject
    lateinit var tabAdapter: TabAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        adapter.clickListener = { onItemClick(it.toRecord()) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        childFragmentManager.setFragmentResultListener(
            requestKey = CreateRecordDialogFragment::class.java.simpleName,
            lifecycleOwner = this
        ) { _, bundle ->
            val amount = bundle.getDouble(KEY_AMOUNT)
            val comment = bundle.getString(KEY_COMMENT)!!
            val isIncome = bundle.getBoolean(KEY_ISINCOME)
            val categoryId = bundle.getInt(KEY_CATEGORY_ID)
            val date = bundle.getLong(KEY_DATE)
            viewModel.createNewRecord(
                Record(Random().nextInt(), isIncome, date, categoryId, amount, comment)
            )
        }
        childFragmentManager.setFragmentResultListener(
            requestKey = RecordInfoDialog::class.java.simpleName,
            lifecycleOwner = this
        ) { _, bundle ->
            val id = bundle.getInt(KEY_RECORD_ID)
            val params = mutableMapOf<String, Any>()
            if (bundle.containsKey(KEY_AMOUNT)) {
                params["count"] = bundle.getDouble(KEY_AMOUNT) //todo change count to amount
            }
            if (bundle.containsKey(KEY_COMMENT)) {
                params[KEY_COMMENT] = bundle.getString(KEY_COMMENT, "")
            }
            if (bundle.containsKey(KEY_ISINCOME)) {
                params["income"] = bundle.getBoolean(KEY_ISINCOME)// todo change income to isIncome
            }
            if (bundle.containsKey(KEY_CATEGORY_ID)) {
                params[KEY_CATEGORY_ID] = bundle.getInt(KEY_CATEGORY_ID)
            }
            if (bundle.containsKey(KEY_DATE)) {
                params[KEY_DATE] = bundle.getLong(KEY_DATE)
            }
            Log.d("testtest", "id = $id and params = $params")
//            val amount = bundle.getDouble(KEY_AMOUNT)
//            val comment = bundle.getString(KEY_COMMENT)!!
//            val isIncome = bundle.getBoolean(KEY_ISINCOME)
//            val categoryId = bundle.getInt(KEY_CATEGORY_ID)
//            val date = bundle.getLong(KEY_DATE)
            viewModel.updateRecord(id.toString(), params)
        }
        childFragmentManager.setFragmentResultListener(
            requestKey = "RecordInfoDialogRemove",
            lifecycleOwner = this
        ) { _, bundle ->
            val id = bundle.getInt(KEY_RECORD_ID)
            Log.d("testtest", "id = $id")

            viewModel.removeRecord(id.toString())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.bottomAppBar)

//        binding.fab.setOnClickListener {
//            val fragment = CreateRecordDialogFragment()
//            fragment.show(childFragmentManager, "CreateRecordDialogFragment")
//        }

        binding.totalVisiblility.setOnClickListener {
            if (binding.totalIncome.isVisible) {
                binding.totalIncome.setInvisible()
                binding.totalSpend.setInvisible()
            } else {
                binding.totalIncome.setVisible()
                binding.totalSpend.setVisible()
            }
        }


        initBottomSheet()
        with(binding) {
            fab.setOnClickListener {
                binding.fab.setImageResource(R.drawable.ic_done)
                binding.transparentBackground.setVisible()
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
            transparentBackground.setOnClickListener {
                binding.fab.setImageResource(R.drawable.ic_add)
                binding.transparentBackground.setGone()
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            bottomSheet.setOnClickListener {
                //to avoid hiding on missclick
            }
        }
    }

    override fun applyViewState(viewState: HomeState) {
        when (viewState) {
            is HomeState.UpdateTabInfoState -> {
//                val listOfROws: MutableList<RecordRow> =
//                    viewState.tabInfo.records.map { it.toRecordRow() }.toMutableList()
//                adapter.setItems(listOfROws)

            }
            is HomeState.InitTabsState -> {
                initTabList(viewState.tabsList)
            }
            is HomeState.RecordCreatedSuccessState -> {
                showSnack("Record was created success")
            }
            is HomeState.RecordCreatedFailureState -> {
                showSnack("Record was created FAIL")
            }
            is HomeState.RecordRemovedState -> {
                if (tabAdapter.selectedTab.tabId == viewState.changedTab.tabId) {
                    Log.d("testtest", "Current tab.")
                    adapter.updateList(viewState.changedTab.records.map { it.toRecordRow() }
                        .toMutableList())
                } else {
                    Log.d("testtest", "Non active tab.")
                    for (tab in tabAdapter.getItems()) {
                        if (tab.tabId == viewState.changedTab.tabId) {
                            Log.d("testtest", "update list in tab ${tab.name}")
                            tab.records = viewState.changedTab.records
                            //todo change other fields
                        }
                    }
                }
            }
            is HomeState.RecordsAddedState -> {
                Log.d("testtest", "Record Added!!!!!!!!!!!!!!!!!!!!!!1")

                if (tabAdapter.selectedTab.tabId == viewState.changedTab.tabId) {
                    Log.d("testtest", "Current tab.")
                    adapter.updateList(viewState.changedTab.records.map { it.toRecordRow() }
                        .toMutableList())
                } else {
                    Log.d("testtest", "Non active tab.")
                    for (tab in tabAdapter.getItems()) {
                        if (tab.tabId == viewState.changedTab.tabId) {
                            Log.d("testtest", "update list in tab ${tab.name}")
                            tab.records = viewState.changedTab.records
                            //todo change other fields(income, spent)
                        }
                    }
                }
            }
            is HomeState.RecordChangeState -> {
                Log.d("testtest", "Record changes!!!!!!!!!!!!!!!!!!!!!!1")
                adapter.updateList(viewState.changedTab.records.map { it.toRecordRow() })
            }
            is HomeState.NewTabAddedState -> {
                Log.d("testtest", "Tab added!!!!!!!!!!!!!!!!!")
                tabAdapter.updateTabList(viewState.tabList)
                openTab(viewState.changedTab)
            }

            is HomeState.TabRemovedState -> {
                Log.d("testtest", "Tab removed!!!!s!!!!!!!!")
                tabAdapter.updateTabList(viewState.tabList)
                if (tabAdapter.getItems().isNotEmpty()) {
                    openTab(tabAdapter.getItems()[tabAdapter.getItems().size - 1])
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.bottom_app_bar, menu);
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val bottomNavDrawerFragment = BottomNavigationDrawerFragment()
                bottomNavDrawerFragment.listener = {menuId ->
                    when(menuId) {
                        R.id.nav_categories -> {
                            navigator.navigateTo(Screens.CATEGORIES)
                        }
                        R.id.nav_settings -> {
                            navigator.navigateTo(Screens.SETTINGS)

                            Log.d("testtest", "open settings")
                        }
                    }
                }
                bottomNavDrawerFragment.show(childFragmentManager, bottomNavDrawerFragment.tag)
            }
            R.id.filter -> {
                Log.d("testtest", "hERe filter click")
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun initTabList(tabList: MutableList<TabInfo>) {
        binding.tabList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        tabAdapter.clickListener = {
            onTabClick(it)
        }
        tabAdapter.setItems(tabList)
        binding.tabList.adapter = tabAdapter
        binding.tabList.scrollToPosition(tabAdapter.getItems().size - 1)
        openTab(tabAdapter.getItems()[tabAdapter.getItems().size - 1])
    }

    private fun onTabClick(tab: TabInfo) {
        openTab(tab)
    }

    private fun onItemClick(record: Record) {
        Log.d("testtest", "open recotd $record")
        RecordInfoDialog.getInstance(record).show(childFragmentManager, "")
    }

    fun openTab(tab: TabInfo) {
        val listOfROws: MutableList<RecordRow> =
            tab.records.map { it.toRecordRow() }.toMutableList()
        adapter.setItems(listOfROws)
        Log.d(
            "testtest",
            "spent = ${tab.totalSpent.toString()}, income = ${tab.totalIncome.toString()}"
        )
        binding.totalSpend.text = tab.totalSpent.toString()
        binding.totalIncome.text = tab.totalIncome.toString()
    }


    fun showSnack(message: String) {
        //todo replace it with base showSnack
        val snackbar = Snackbar
            .make(
                binding.root,
                message,
                Snackbar.LENGTH_LONG
            )
        snackbar.show()
    }

    private fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)
        bottomSheetBehavior.isGestureInsetBottomIgnored = true
        bottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback)
        binding.bottomSheet.onCategoriesClickListener = {
//            viewModel.onCategoryInCreateClicked()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bottomSheetBehavior.removeBottomSheetCallback(bottomSheetCallback)
    }

    private fun initRecyclerView() {
        binding.recordList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recordList.addItemDecoration(ItemDecorator())
        binding.recordList.adapter = adapter
    }
}