package com.intemdox.moneysaver.usecase.login

import com.intemdox.moneysaver.base.BaseViewAction
import com.intemdox.moneysaver.base.BaseViewState


sealed class LoginState : BaseViewState {
    object EmptyState : LoginState()
}

sealed class LoginActions : BaseViewAction