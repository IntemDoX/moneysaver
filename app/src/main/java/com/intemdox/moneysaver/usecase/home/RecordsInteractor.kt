package com.intemdox.moneysaver.usecase.home

import com.intemdox.moneysaver.CreateNewRecordResult
import com.intemdox.moneysaver.GetAllRecordsResult
import com.intemdox.moneysaver.RemoveRecordResult
import com.intemdox.moneysaver.UpdateRecordResult
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.model.Record

interface RecordsInteractor {
    suspend fun createNewRecord(record: Record): CreateNewRecordResult
    suspend fun updateRecord(id: String, params: MutableMap<String, Any>): UpdateRecordResult
    suspend fun removeRecord(id: String): RemoveRecordResult
    suspend fun getAllRecords(): GetAllRecordsResult
    suspend fun subscribeOnRecordsChanges(
        onNewAdded: (record: Record) -> Unit,
        onModified: (record: Record) -> Unit,
        onModifiedWithDate: (record: Record) -> Unit,
        onRemoved: (record: Record) -> Unit
    )
}