package com.intemdox.moneysaver.usecase.createRecord

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.intemdox.moneysaver.databinding.ItemCategoryBinding
import com.intemdox.moneysaver.model.Category
import javax.inject.Inject

class CategoriesAdapter @Inject constructor() :
    RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder>() {
    var clickListener: ((Category?) -> Unit)? = null
    private val list: MutableList<Category> = mutableListOf()
    private var checked = -1
    var lastSelectedHolder: CategoryViewHolder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(list: MutableList<Category>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun getItemByPosition(position: Int): Category {
        return list[position]
    }

    fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: Category, position: Int) {
        list.add(position, item)
        notifyItemInserted(position)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(list[position], clickListener)
        holder.binding.title.text = list[position].name
        clickListener?.let { listener ->
            holder.itemView.setOnClickListener {
                if (checked != position) {
                    lastSelectedHolder?.let {
                        it.itemView.animate().scaleX(1f)
                        it.itemView.animate().scaleY(1f)
                    }
                    checked = position
                    lastSelectedHolder = holder
                    holder.itemView.animate().scaleX(1.5f)
                    holder.itemView.animate().scaleY(1.5f)
                    listener((list[position]))
                } else {
                    checked = -1
                    lastSelectedHolder = null
                    holder.itemView.animate().scaleX(1f)
                    holder.itemView.animate().scaleY(1f)
                    listener(null)
                }
            }
        }
    }

    class CategoryViewHolder(var binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(category: Category, clickListener: ((Category) -> Unit)?) {
            setColor(category.color)

        }

        private fun setColor(color: String) {
            val layerList = binding.container.background as GradientDrawable
            layerList.setColor(Color.parseColor(color))
        }


    }
}