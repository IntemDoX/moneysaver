package com.intemdox.moneysaver.usecase.login

import androidx.fragment.app.viewModels
import com.intemdox.moneysaver.base.BaseFragment
import com.intemdox.moneysaver.databinding.FragmentLoginBinding

class LoginFragment() :
    BaseFragment<LoginState, LoginActions, LoginViewModel, FragmentLoginBinding>() {

     override val viewModel: LoginViewModel by viewModels()
    override var _binding: FragmentLoginBinding?
        get() = TODO("Not yet implemented")
        set(value) {}

    override fun applyViewState(viewState: LoginState) {
        TODO("Not yet implemented")
    }

}