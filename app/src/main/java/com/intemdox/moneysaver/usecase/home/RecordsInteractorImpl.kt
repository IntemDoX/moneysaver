package com.intemdox.moneysaver.usecase.home

import android.util.Log
import com.intemdox.moneysaver.*
import com.intemdox.moneysaver.coroutines.backgroundTask
import com.intemdox.moneysaver.coroutines.ioTaskAsync
import com.intemdox.moneysaver.model.Record
import com.intemdox.moneysaver.model.TabInfo
import com.intemdox.moneysaver.repository.FirestoreRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import javax.inject.Inject

@ExperimentalCoroutinesApi
open class RecordsInteractorImpl
@Inject constructor(var firestoreRepositoryImpl: FirestoreRepository): RecordsInteractor {

    override suspend  fun createNewRecord(record: Record): CreateNewRecordResult = backgroundTask {
        val data = firestoreRepositoryImpl.writeRecordToDB(record)
        data.e?.let {
            return@backgroundTask CreateNewRecordFailure(it)
        }
        return@backgroundTask CreateNewRecordSuccess
    }

    override suspend fun updateRecord(id: String, params: MutableMap<String, Any>): UpdateRecordResult =
        backgroundTask {
            val data = firestoreRepositoryImpl.updateRecord(id, params)
            data.e?.let {
                return@backgroundTask UpdateRecordError(it)
            }
            return@backgroundTask UpdateRecordSuccess
        }

    override suspend fun removeRecord(id: String): RemoveRecordResult = backgroundTask {
        val data = firestoreRepositoryImpl.removeRecord(id)
        data.e?.let {
            return@backgroundTask RemoveRecordError(it)
        }
        return@backgroundTask RemoveRecordSuccess
    }

    override suspend fun getAllRecords(): GetAllRecordsResult = backgroundTask {
        Log.d("testtest", "start get records")
        val result = firestoreRepositoryImpl.getAllRecords()
        val resultTabs = ioTaskAsync {
            prepareTabLayout(result.data!!)
        }.await()
        Log.d("testtest", "get all records success")
        return@backgroundTask GetAllRecordsSuccess(resultTabs)
    }

    override suspend fun subscribeOnRecordsChanges(
        onNewAdded: (record: Record) -> Unit,
        onModified: (record: Record) -> Unit,
        onModifiedWithDate: (record: Record) -> Unit,
        onRemoved: (record: Record) -> Unit
    ) {
        firestoreRepositoryImpl.subscribeOnFireStore(onNewAdded = onNewAdded, onModified, onModifiedWithDate, onRemoved)
    }

    private fun prepareTabLayout(list: MutableList<Record>): MutableList<TabInfo> {
        val resultTabs = mutableListOf<TabInfo>()
        var isFinished = false
        var i = 0
        var oldDate = Date().time

        while (!isFinished) {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - i)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.HOUR, 0)
            calendar.set(Calendar.DAY_OF_MONTH, 1) //first day of month
            val result = mutableListOf<Record>()
            val date = calendar.timeInMillis
            var isAdded = false
            var monthSumm = 0.0
            var monthIncome = 0.0
            for (item in list) {
                if (item.date in (date + 1) until oldDate) {
                    isAdded = true
                    result.add(item)
                    if (item.isIncome) {
                        monthIncome += item.count
                    } else {
                        monthSumm += item.count
                    }
                }
            }
            i++
            if (isAdded) {
                val tabName = String.format(Locale.getDefault(), "%tb%tY", calendar, calendar)
                val tabId = String.format(Locale.US, "%tb%tY", calendar, calendar)
                Log.d("testtest", "tabName = $tabName, tabId = $tabId")
                val tab = TabInfo(
                    name = tabName,
                    totalIncome = monthIncome,
                    totalSpent = monthSumm,
                    records = result,
                    tabId = tabId
                )
                resultTabs.add(0, tab)
            }
            oldDate = date
            var resultSize = 0
            for (i in resultTabs) {
                resultSize += i.records.size
            }
            isFinished = list.size == resultSize
        }
        return resultTabs
    }
}