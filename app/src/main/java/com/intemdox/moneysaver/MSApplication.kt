package com.intemdox.moneysaver

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MSApplication : Application()