package com.intemdox.moneysaver.utils

import androidx.recyclerview.widget.DiffUtil
import com.intemdox.moneysaver.model.TabInfo

class DiffCallbackTabs(
    private var newRecords: MutableList<TabInfo>,
    private var oldRecords: MutableList<TabInfo>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newRecords[newItemPosition].tabId == oldRecords[oldItemPosition].tabId
    }

    override fun getOldListSize(): Int {
        return oldRecords.size
    }

    override fun getNewListSize(): Int {
        return newRecords.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newRecords[newItemPosition] == oldRecords[oldItemPosition]
    }
}