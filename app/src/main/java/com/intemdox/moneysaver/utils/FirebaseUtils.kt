package com.intemdox.moneysaver.utils

import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.model.Record

class FirebaseUtils {
    companion object {
        fun DSToRecord(ds: DocumentSnapshot): Record {
            val record = Record()
            if (ds.contains("id")) {
                record.id = ds.getLong("id")!!.toInt()
//                Log.d("testtest", "id = ${record.id}")
            }
            if (ds.contains("income")) {
                record.isIncome = ds.getBoolean("income")!!
            }
            if (ds.contains("date")) {
                record.date = ds.getLong("date")!!
            }

            if (ds.contains("comment")) {
                record.comment = ds.getString("comment")
            }
            if (ds.contains("count")) {
                record.count = ds.getDouble("count")!!
            }
            if (ds.contains("categoryId")) {
                record.categoryId = ds.getLong("categoryId")!!.toInt()
            }
            return record
        }
        
        fun DSToCategory(ds: DocumentSnapshot): Category {
            val category = Category()
            if (ds.contains("id")) {
                category.id = ds.getLong("id")!!.toInt()
            }
            if (ds.contains("color")) {
                category.color = ds.getString("color")!!
            }
            if (ds.contains("name")) {
                category.name = ds.getString("name")!!
            }
            return category
        }
    }
}