package com.intemdox.moneysaver.repository

import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.model.Record

interface FirestoreRepository {
    var categories: MutableList<Category>
    suspend fun writeRecordToDB(record: Record): FirebaseCreateResult
    suspend fun getAllRecords(): FirebaseGetResult<MutableList<Record>>
    suspend fun updateRecord(id: String, params: MutableMap<String, Any>): FirebaseUpdateResult
    suspend fun removeRecord(id: String): FirebaseRemoveResult
    suspend fun subscribeOnFireStore(
        onNewAdded: (record: Record) -> Unit,
        onModified: (record: Record) -> Unit,
        onModifiedWithDate: (record: Record) -> Unit,
        onRemoved: (record: Record) -> Unit
    )

    suspend fun subscribeOnCategoriesChanges(
        onNewAdded: (category: Category) -> Unit,
        onModified: (category: Category) -> Unit,
        onRemoved: (category: Category) -> Unit
    )

    suspend fun getAllCategories(): FirebaseGetCategoriesResult
    suspend fun createCategory(category: Category): FirebaseCreateCategoryResult
    suspend fun removeCategory(id: String): FirebaseRemoveCategoryResult
    suspend fun updateCategory(category: Category, params: MutableMap<String, Any>): FirebaseUpdateCategoryResult
    fun getCategoryById(id: String): Category?

}