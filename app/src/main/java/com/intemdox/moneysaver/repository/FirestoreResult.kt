package com.intemdox.moneysaver.repository

import com.intemdox.moneysaver.model.Category


data class FirebaseGetResult<T>(var data: T?, var e: Throwable? = null)
data class FirebaseCreateResult(var e: Throwable? = null)
data class FirebaseUpdateResult(var e: Throwable? = null)
data class FirebaseRemoveResult(var e: Throwable? = null)
sealed class FirebaseGetCategoriesResult
data class FirebaseGetCategoriesSuccess(var categories: MutableList<Category>) :
    FirebaseGetCategoriesResult()

data class FirebaseGetCategoriesError(var e: Throwable) :
    FirebaseGetCategoriesResult()


sealed class FirebaseCreateCategoryResult
data class FirebaseCreateCategorySuccess(val category: Category) : FirebaseCreateCategoryResult()
data class FirebaseCreateCategoryError(val e: Throwable) : FirebaseCreateCategoryResult()
data class FirebaseRemoveCategoryResult(var e: Throwable? = null)

sealed class FirebaseUpdateCategoryResult
data class FirebaseUpdateCategorySuccess(val category: Category) : FirebaseUpdateCategoryResult()
data class FirebaseUpdateCategoryError(val e: Throwable) : FirebaseUpdateCategoryResult()