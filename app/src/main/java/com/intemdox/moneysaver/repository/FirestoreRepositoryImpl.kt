package com.intemdox.moneysaver.repository

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.model.Record
import com.intemdox.moneysaver.utils.FirebaseUtils
import com.intemdox.moneysaver.utils.toShortDate
import kotlinx.coroutines.*
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.resumeWithException


@ExperimentalCoroutinesApi
open class FirestoreRepositoryImpl @Inject constructor() : FirestoreRepository {
    private val database: FirebaseFirestore = Firebase.firestore
    private var isSubscriptionInitialized = false
    override var categories: MutableList<Category> = mutableListOf()

    init {
        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true)
    }

    private fun getUserDocument(): DocumentReference {
        return database.collection("users").document("user001")
    }

    private fun getUserRecords(): CollectionReference {
        return getUserDocument().collection("records")
    }

    private fun getUserCategories(): CollectionReference {
        return getUserDocument().collection("categories")
    }

    /**
     * To find date of first record. We need it to find out how much tabs we should create
     */
    private fun getFirstRecord(): Query {
        return getUserRecords().orderBy("date", Query.Direction.ASCENDING).limit(1)
    }

    private fun getLast3Month(): Query {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 4)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.HOUR, 0)
        calendar.set(Calendar.DAY_OF_MONTH, 1) //first day of month
        Log.d("testtest", calendar.timeInMillis.toShortDate())
        return getUserRecords().orderBy("date", Query.Direction.DESCENDING)
            .whereGreaterThan("date", calendar.timeInMillis)
    }

    suspend fun getInitialRecords(): FirebaseGetResult<MutableList<Record>> {
        var result: FirebaseGetResult<MutableList<Record>>? = null
        val resultList = mutableListOf<Record>()
        val firstRecordTask = getFirstRecord().get()
        val last3MonthTask = getLast3Month().get()
        Tasks.whenAllSuccess<QuerySnapshot>(last3MonthTask, firstRecordTask).addOnSuccessListener {
            it.forEach { documentSnapshot ->
                documentSnapshot.forEach { d ->
                    val item = FirebaseUtils.DSToRecord(d)
                    if (!resultList.contains(item)) {
                        resultList.add(item)
                    }
                }
            }
            result = FirebaseGetResult(resultList)
        }.addOnFailureListener {
            result = FirebaseGetResult(null, it)
        }.asDeferred().await()

        Log.d("testtest", "result = $result")
        return result!!
    }

    override suspend fun getAllRecords(): FirebaseGetResult<MutableList<Record>> {
        var result: FirebaseGetResult<MutableList<Record>>? = null
        val resultList = mutableListOf<Record>()
        getUserRecords().orderBy("date", Query.Direction.DESCENDING).get().addOnCompleteListener {
            it.result?.documents?.forEach { documentSnapshot ->
                resultList.add(FirebaseUtils.DSToRecord(documentSnapshot))
            }
            result = FirebaseGetResult(resultList)
        }.addOnFailureListener {
            result = FirebaseGetResult(null, it)
        }.asDeferred().await()
        return result!!
    }


    override suspend fun writeRecordToDB(record: Record): FirebaseCreateResult {
        var result: FirebaseCreateResult? = null
        getUserRecords().document(record.id.toString()).set(record).addOnSuccessListener {
            result = FirebaseCreateResult()
        }.addOnFailureListener {
            result = FirebaseCreateResult(it)
        }.asDeferred().await()
        return result!!
    }

    override suspend fun createCategory(category: Category): FirebaseCreateCategoryResult {
        var result: FirebaseCreateCategoryResult = FirebaseCreateCategorySuccess(category)
        getUserCategories().document(category.id.toString()).set(category).addOnSuccessListener {
            result = FirebaseCreateCategorySuccess(category)
        }.addOnFailureListener {
            result = FirebaseCreateCategoryError(it)
        }.asDeferred().await()
        return result
    }

    override suspend fun updateRecord(
        id: String,
        params: MutableMap<String, Any>
    ): FirebaseUpdateResult {
        var result: FirebaseUpdateResult? = null
        getUserRecords().document(id).update(params)
            .addOnSuccessListener {
                result = FirebaseUpdateResult()
            }.addOnFailureListener {
                result = FirebaseUpdateResult(it)
            }.asDeferred().await()
        return result!!
    }

    override suspend fun updateCategory(category: Category, params: MutableMap<String, Any>): FirebaseUpdateCategoryResult {
        var result: FirebaseUpdateCategoryResult? = null
        getUserRecords().document(category.id.toString()).update(params)
            .addOnSuccessListener {
                result = FirebaseUpdateCategorySuccess(category)
            }.addOnFailureListener {
                result = FirebaseUpdateCategoryError(it)
            }.asDeferred().await()
        return result!!
    }

    override suspend fun removeRecord(id: String): FirebaseRemoveResult {
        var result: FirebaseRemoveResult? = null
        getUserRecords().document(id).delete()
            .addOnSuccessListener {
                result = FirebaseRemoveResult()
            }.addOnFailureListener {
                result = FirebaseRemoveResult(it)
            }.asDeferred().await()
        return result!!
    }

    override suspend fun removeCategory(id: String): FirebaseRemoveCategoryResult {
        var result = FirebaseRemoveCategoryResult()
        getUserRecords().document(id).delete()
            .addOnSuccessListener {

            }.addOnFailureListener {
                result = FirebaseRemoveCategoryResult(it)
            }.asDeferred().await()
        return result
    }

    override fun getCategoryById(id: String): Category? {
        Log.d("testtest", "categoryId = $id")
        for (category in categories) {
            if (category.id.toString() == id) {
                return category
            }
        }
        return null
    }

    override suspend fun getAllCategories(): FirebaseGetCategoriesResult {
        var result: FirebaseGetCategoriesResult = FirebaseGetCategoriesError(Exception())
        getUserCategories().get().addOnSuccessListener {
            val resultList = mutableListOf<Category>()
            it?.documents?.forEach { documentSnapshot ->
                val cat = FirebaseUtils.DSToCategory(documentSnapshot)
                resultList.add(cat)
                Log.d("testtest", "added cat = $cat")

            }
            categories = resultList
            Log.d("testtest", "here!!!!! init categories list = $categories")
            result = FirebaseGetCategoriesSuccess(categories = categories)
        }.addOnFailureListener {
            result = FirebaseGetCategoriesError(e = it)
        }.asDeferred().await()
        return result
    }

    suspend fun getAllRecordsOrdered() {

        getUserRecords().orderBy("date", Query.Direction.DESCENDING).get().addOnSuccessListener {

        }.addOnFailureListener {

        }
    }


    suspend fun removeDocumentById(id: String) {
        getUserRecords().document(id).delete()
    }

    override suspend fun subscribeOnFireStore(
        onNewAdded: (record: Record) -> Unit,
        onModified: (record: Record) -> Unit,
        onModifiedWithDate: (record: Record) -> Unit,
        onRemoved: (record: Record) -> Unit
    ) {
        getUserRecords().addSnapshotListener(
            MetadataChanges.EXCLUDE
        ) { querySnapshot, e ->
            if (e != null) {
                Log.w("testtest", "listen:error", e)
                //todo return
            }
            if (isSubscriptionInitialized) {
                for (dc in querySnapshot!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> onNewAdded(FirebaseUtils.DSToRecord(dc.document))
                        DocumentChange.Type.MODIFIED -> {
                            if (dc.newIndex == dc.oldIndex) {
                                Log.d("testtest", "just modified ")
                                onModified(FirebaseUtils.DSToRecord(dc.document))
                            } else {
                                Log.d("testtest", "modified with date ")
                                onModifiedWithDate(FirebaseUtils.DSToRecord(dc.document))
                            }
                        }
                        DocumentChange.Type.REMOVED -> onRemoved(FirebaseUtils.DSToRecord(dc.document))
                    }
                }
            } else {
                Log.d("testtest", "here 005")
                isSubscriptionInitialized = true
            }
        }
    }

    override suspend fun subscribeOnCategoriesChanges(
        onNewAdded: (category: Category) -> Unit,
        onModified: (category: Category) -> Unit,
        onRemoved: (category: Category) -> Unit
    ) {
        getUserRecords().addSnapshotListener(
            MetadataChanges.EXCLUDE
        ) { querySnapshot, e ->
            if (e != null) {
                Log.w("testtest", "listen:error", e)
                //todo return
            }
            if (isSubscriptionInitialized) {
                for (dc in querySnapshot!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> onNewAdded(FirebaseUtils.DSToCategory(dc.document))
                        DocumentChange.Type.MODIFIED -> {
                            Log.d("testtest", "just modified ")
                            onModified(FirebaseUtils.DSToCategory(dc.document))
                        }
                        DocumentChange.Type.REMOVED -> onRemoved(FirebaseUtils.DSToCategory(dc.document))
                    }
                }
            } else {
                Log.d("testtest", "here 005")
                isSubscriptionInitialized = true
            }
        }
    }


    fun lt(place: String) {
        Log.d("testtest", "$place is executing on thread : ${Thread.currentThread().name}")
    }

    fun <T> Task<T>.asDeferred(): Deferred<T> {
        val deferred = CompletableDeferred<T>()

        deferred.invokeOnCompletion {
            if (deferred.isCancelled) {
                // optional, handle coroutine cancellation however you'd like here
            }
        }
        this.addOnSuccessListener { result -> deferred.complete(result) }
        this.addOnFailureListener { exception -> deferred.completeExceptionally(exception) }

        return deferred
    }


    suspend fun <T> Task<T>.await(): Result<T> {
        if (isComplete) {
            val e = exception
            return if (e == null) {
                if (isCanceled) {
                    Result.Canceled(CancellationException("Task $this was cancelled normally."))
                } else {
                    @Suppress("UNCHECKED_CAST")
                    (Result.Success(result as T))
                }
            } else {
                Result.Error(e)
            }
        }

        return suspendCancellableCoroutine { cont ->
            addOnCompleteListener {
                val e = exception
                if (e == null) {
                    @Suppress("UNCHECKED_CAST")
                    if (isCanceled) cont.cancel() else cont.resume(Result.Success(result as T), {
                        //???
                    })
                } else {
                    cont.resumeWithException(e)
                }
            }
        }
    }
}

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>() // Status success and data of the result
    data class Error(val exception: Exception) : Result<Nothing>() // Status Error an error message
    data class Canceled(val exception: Exception?) : Result<Nothing>() // Status Canceled

    // string method to display a result for debugging
    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            is Canceled -> "Canceled[exception=$exception]"
        }
    }
}
