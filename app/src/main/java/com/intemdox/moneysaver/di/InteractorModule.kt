package com.intemdox.moneysaver.di

import com.intemdox.moneysaver.usecase.categories.CategoriesInteractor
import com.intemdox.moneysaver.usecase.categories.CategoriesInteractorImpl
import com.intemdox.moneysaver.usecase.home.RecordsInteractor
import com.intemdox.moneysaver.usecase.home.RecordsInteractorImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@InstallIn(ApplicationComponent::class)
@Module
abstract class InteractorModule {

    @Binds
    abstract fun provideRecordInteractor(impl: RecordsInteractorImpl): RecordsInteractor

    @Binds
    abstract fun provideCategoriesInteractor(impl: CategoriesInteractorImpl): CategoriesInteractor
}