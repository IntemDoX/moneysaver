package com.intemdox.moneysaver.di

import com.intemdox.moneysaver.repository.FirestoreRepository
import com.intemdox.moneysaver.repository.FirestoreRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun provideDataRepository(impl: FirestoreRepositoryImpl): FirestoreRepository
}