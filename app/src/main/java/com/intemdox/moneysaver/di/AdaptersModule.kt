package com.intemdox.moneysaver.di

import com.intemdox.moneysaver.usecase.categories.ColorsAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@InstallIn(ApplicationComponent::class)
@Module
class AdaptersModule {
    @Provides
    fun provideColorAdapter(): ColorsAdapter {
        return ColorsAdapter()
    }
}