package com.intemdox.moneysaver

import com.intemdox.moneysaver.model.Category
import com.intemdox.moneysaver.model.TabInfo

sealed class GetAllRecordsResult
data class GetAllRecordsSuccess(val records: MutableList<TabInfo>) : GetAllRecordsResult()
data class GetAllRecordsError(val e: Throwable) : GetAllRecordsResult()

sealed class CreateNewRecordResult
object CreateNewRecordSuccess : CreateNewRecordResult()
data class CreateNewRecordFailure(val e: Throwable) : CreateNewRecordResult()

sealed class UpdateRecordResult
object UpdateRecordSuccess : UpdateRecordResult()
data class UpdateRecordError(val e: Throwable) : UpdateRecordResult()

sealed class RemoveRecordResult
object RemoveRecordSuccess : RemoveRecordResult()
data class RemoveRecordError(val e: Throwable) : RemoveRecordResult()

sealed class CreateCategoryResult
data class CreateCategorySuccess(var category: Category) : CreateCategoryResult()
data class CreateCategoryError(val e: Throwable) : CreateCategoryResult()

sealed class UpdateCategoryResult
data class UpdateCategorySuccess(val category: Category) : UpdateCategoryResult()
data class UpdateCategoryError(val e: Throwable) : UpdateCategoryResult()

sealed class RemoveCategoryResult
data class RemoveCategorySuccess(val category: Category) : RemoveCategoryResult()
data class RemoveCategoryError(val e: Throwable) : RemoveCategoryResult()

sealed class GetAllCategoriesResult
data class GetAllCategoriesSuccess(val categories: MutableList<Category>) : GetAllCategoriesResult()
data class GetAllCategoriesError(val e: Throwable) : GetAllCategoriesResult()
