package com.intemdox.moneysaver

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.intemdox.moneysaver.model.Record
import com.intemdox.moneysaver.repository.FirestoreRepository
import com.intemdox.moneysaver.usecase.home.HomeState
import com.intemdox.moneysaver.usecase.home.HomeViewModel
import com.intemdox.moneysaver.usecase.home.RecordsInteractor
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.lang.NullPointerException


class MyUnitTest {
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Mock
    private lateinit var recordsINteractor: RecordsInteractor

    @Mock
    private lateinit var stateObserver: Observer<HomeState>

    @Mock
    private lateinit var repo: FirestoreRepository

    private lateinit var viewModel: HomeViewModel

    @get:Rule
    public var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate) //read about it https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-test/
        MockitoAnnotations.initMocks(this) //read doc
        viewModel = HomeViewModel(repo, recordsINteractor)
        viewModel.stateData.observeForever(stateObserver)

    }

    @Test
    fun `test remove record success`() = runBlockingTest {
        whenever(recordsINteractor.updateRecord(any(), any())).thenReturn(UpdateRecordSuccess)
        assert(viewModel.stateData.value == HomeState.EmptyState)
        viewModel.updateRecord("", mutableMapOf())
        println("Value1 = ${viewModel.stateData.value}")
        verify(stateObserver).onChanged(HomeState.RecordUpdatedSuccessState)
        println("Value2 = ${viewModel.stateData.value}")
        assert(viewModel.stateData.value is HomeState.RecordUpdatedSuccessState)
    }
    @Test
    fun `test create new record success`() = runBlockingTest {
        whenever(recordsINteractor.createNewRecord(any())).thenReturn(CreateNewRecordSuccess)
        assert(viewModel.stateData.value == HomeState.EmptyState)
        viewModel.createNewRecord(Record())
        println("Value1 = ${viewModel.stateData.value}")
        verify(stateObserver).onChanged(HomeState.RecordCreatedSuccessState)
        println("Value2 = ${viewModel.stateData.value}")
        assert(viewModel.stateData.value is HomeState.RecordCreatedSuccessState)
    }


    @Test
    fun `test create new record fail`() = runBlockingTest {
        val e = NullPointerException("LOLKEK")
        whenever(recordsINteractor.createNewRecord(any())).thenReturn(CreateNewRecordFailure(e))
        assert(viewModel.stateData.value == HomeState.EmptyState)
        viewModel.createNewRecord(Record())
        println("Value1 = ${viewModel.stateData.value}")
        verify(stateObserver).onChanged(eq(HomeState.RecordCreatedFailureState(e)))
        println("Value2 = ${viewModel.stateData.value}")
        assert(viewModel.stateData.value is HomeState.RecordCreatedFailureState)
    }
//
//    @Test
//    fun `test create new record fail`() = runBlockingTest {
//        val e = NullPointerException("LOLKEK")
//        whenever(recordsINteractor.getAllRecords()).thenReturn(GetAllRecordsSuccess(
//            mutableListOf()))
//        assert(viewModel.stateData.value == HomeState.EmptyState)
//        viewModel.createNewRecord(Record())
//        println("Value1 = ${viewModel.stateData.value}")
//        verify(stateObserver).onChanged(eq(HomeState.InitTabsState(mutableListOf())))
//        println("Value2 = ${viewModel.stateData.value}")
//        assert(viewModel.stateData.value is HomeState.InitTabsState)
//    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

}