package com.intemdox.moneysaver

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.intemdox.moneysaver.repository.FirebaseCreateResult
import com.intemdox.moneysaver.repository.FirestoreRepository
import com.intemdox.moneysaver.usecase.home.RecordsInteractor
import com.intemdox.moneysaver.usecase.home.RecordsInteractorImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.*
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import kotlin.coroutines.CoroutineContext

@ExperimentalCoroutinesApi
@ObsoleteCoroutinesApi
class RecordsInteractorTest {
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)
    private val networkContext: CoroutineContext = testDispatcher

    @Mock
    private lateinit var repo: FirestoreRepository

    @get:Rule
    public var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var interactor: RecordsInteractor

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate) //read about it https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-test/
        MockitoAnnotations.initMocks(this) //read doc
        interactor = RecordsInteractorImpl(repo)
    }


    @Test
    fun `test create new record`() = testScope.runBlockingTest() {
        println("ss")

        whenever(repo.writeRecordToDB(any())).thenReturn(FirebaseCreateResult())
        println("sssss")
        testScope.async {
            val result = interactor.createNewRecord(any())
            println("Value1 = $result")
            assert(result == CreateNewRecordSuccess)

        }.await()


//        viewModel.updateRecord("", mutableMapOf())
//        verify(stateObserver).onChanged(HomeState.RecordUpdatedSuccessState)
//        println("Value2 = ${viewModel.stateData.value}")
//        assert(viewModel.stateData.value is HomeState.RecordUpdatedSuccessState)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

}